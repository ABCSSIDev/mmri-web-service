<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class System extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'icon' => $this->icon,
            'route' => $this->route,
            'value' => $this->id,
            'text' => $this->name,

        ];
    }

    public static function errorResponse($description){
        return [ "error" => true, "desc" => $description];
    }
}
