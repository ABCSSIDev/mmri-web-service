<?php

use Illuminate\Database\Seeder;

class SystemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        \App\System::create([
            'name' => 'HRIS/Payroll',
            'description' => 'Human Resource Management',
        ]);

        \App\System::create([
            'name' => 'CRM',
            'description' => 'Customer Relation Management',
        ]);

        \App\System::create([
            'name' => 'Inventory',
            'description' => 'Inventory and Operations Management',
        ]);

        \App\System::create([
            'name' => 'Accounting',
            'description' => 'Acounting Management',
        ]);

        \App\System::create([
            'name' => 'Administration',
            'description' => 'Application Settings',
        ]);
    }
}
