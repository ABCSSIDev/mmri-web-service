<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::group([
    'prefix' => 'auth'
],function (){
    Route::post('login','Auth\LoginController');
    Route::group([
        'middleware' => 'auth:api'
    ],function (){
        Route::get('me','Auth\MeController');
        Route::post('logout', 'Auth\LogoutController');
    });
});
Route::resource('system','SystemController');
Route::get('/module/get_modules/{system_id}','ModuleController@getModules');
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
